import { Component, OnInit } from '@angular/core';
import {StatisticsService} from '../../services/statistics.service';

@Component({
  selector: 'app-statistics-page',
  templateUrl: './statistics-page.component.html',
  styleUrls: ['./statistics-page.component.scss']
})
export class StatisticsPageComponent implements OnInit {

  dataSource: any[] = [];
  displayedColumns: string[] = [
    'publisher',
    'advertiserName',
    'offerName',
    'amount',
    'comission',
    'earnings'
  ]

  constructor(private statisticsService: StatisticsService) { }

  ngOnInit() {
    this.statisticsService.getRows().subscribe(res => {
      this.dataSource = res;
    })
  }

}
