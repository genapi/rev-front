import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StatisticsPageComponent} from './pages/statistics-page/statistics-page.component';

const routes: Routes = [
  {
    path: '',
    component: StatisticsPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
